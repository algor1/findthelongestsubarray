/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package com.mycompany.midtermalgorithms3;

import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author ASUS
 */
public class FindThelongestSubarray {

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        int size = kb.nextInt();
        int[] arr = new int[size]; 
        for (int i = 0 ; i<size;i++){
            arr[i]= kb.nextInt();
        }
        System.out.println("Input");
        System.out.println(Arrays.toString(arr));
        int countmax = 0;
        int start = 0;
        for (int i = 0; i < arr.length; i++) {
            int count = 0;
            int n1 = arr[i];
            for (int k = i + 1; k < arr.length; k++) {
                n1++;
                if (n1 == arr[k]) {
                    count++;
                } else {
                    break;
                }
            }
            if (count > countmax) {
                countmax = count;
                start = i;
            }
            if (i == arr.length-1) {
                System.out.println("Output:");
                for (int k = start; k < (start + countmax+1); k++) {
                    System.out.print(arr[k]+" ");
                }
            }
        }
    }
}
